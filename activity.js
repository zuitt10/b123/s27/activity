let http = require('http')

let courses = [];

http.createServer(function(req,res){

// view courses
if(req.url === '/courses' && req.method === 'GET'){
	console.log(req.body)
	res.writeHead(200,{'Content-Type': 'application/json'})
	res.end(JSON.stringify(courses))

// Add a course in courses array of object
}	else if(req.url === "/courses" && req.method === "POST"){

	let requestBody = "";

	req.on('data',function(data){

		requestBody += data;
	})

	req.on('end', function(){

		console.log(requestBody);
		requestBody = JSON.parse(requestBody);

		let newCourses = {

			name: requestBody.name,
			description: requestBody.description,
			price:requestBody.price
		}

		courses.push(newCourses);
		console.log(courses)

		res.writeHead(200,{"Content-Type": "application/json"});

		res.end(`Course ${newCourses.name} Added`);

	})
// Find a course name registered into the courses array of object
} else if(req.url === "/courses/getSingleCourse" && req.method === "POST"){


	let requestBody = "";

	req.on('data',function(data){

		requestBody += data;
	})

	req.on('end',function(){
		console.log(requestBody);
		requestBody = JSON.parse(requestBody);

		let foundCourse = courses.find((course)=>{
			return course.name === requestBody.name;
		})

		console.log(foundCourse);

		if(foundCourse !== undefined){

		res.writeHead(200,{"Content-Type": "application/json"});
		res.end(JSON.stringify(foundCourse));
	} else {
		res.writeHead(404,{"Content-Type": "application/json"});
		res.end("Course Not Found.");
	}

	})
	
// Delete the last item in courses array of object
} else if(req.url === "/courses/deleteSingleCourse" && req.method === "DELETE"){
	courses.pop();
	console.log(courses)
	res.writeHead(200,{"Content-Type": "application/json"});
	res.end(`Course Deleted.`);
}

}).listen(8000);
console.log('Server is running at port 8000')